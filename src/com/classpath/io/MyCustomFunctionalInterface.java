package com.classpath.io;

@FunctionalInterface
public interface MyCustomFunctionalInterface<T> {

    boolean test( T t);
}