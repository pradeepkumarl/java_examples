package com.classpath.io;


import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;

public class SerializationDemo {

    public static void main(String[] args)  {

        /*Product product = new Product(12, 45_000, "IPhone");
        try {
            FileOutputStream outputStream = new FileOutputStream(("D:\\product.ser"));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(product);
        } catch (Exception e){
            e.printStackTrace();
        }*/

        try {
            FileInputStream inputStream = new FileInputStream(("D:\\product.ser"));
            ObjectInputStream objectOutputStream = new ObjectInputStream(inputStream);
            Product product = (Product) objectOutputStream.readObject();
            System.out.println(product);

            Predicate<String> isValid = (str) -> str.length() > 3 ;

        } catch (Exception e){
            e.printStackTrace();
        }

        MyCustomFunctionalInterface<String> valid = (str) -> str.length() > 4;
        Date data = new Date();
        Calendar calendar = Calendar.getInstance();
    }
}