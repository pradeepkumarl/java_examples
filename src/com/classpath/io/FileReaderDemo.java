package com.classpath.io;

import java.io.*;

public class FileReaderDemo {

    public static void main(String[] args) {

        writeFile("D:\\data.txt");
        readFile("D:\\data.txt");
    }

    private static final void readFile(String fileName){
        try(BufferedReader reader  = new BufferedReader(new FileReader(fileName))){
            boolean flag = true;
            while(flag){
                String line = reader.readLine();
                if( line == null){
                    flag = false;
                    continue;
                } else {
                    System.out.print(line);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("FIle with the given name and location is not present :: ");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Cannot establish the IO connection");
            e.printStackTrace();
        }
    }
    private static final void writeFile(String fileName){
        try(BufferedWriter writer  = new BufferedWriter(new FileWriter(fileName))){
            writer.append("Some randome text :::: ");
        } catch (FileNotFoundException e) {
            System.out.println("FIle with the given name and location is not present :: ");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Cannot establish the IO connection");
            e.printStackTrace();
        }
    }
}