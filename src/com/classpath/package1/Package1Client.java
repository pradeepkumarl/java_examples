package com.classpath.package1;

public class Package1Client {

    public static void main(String[] args) {
        ModifierDemo modifierDemo = new ModifierDemo();
        System.out.println(modifierDemo.publicVariable);
        System.out.println(modifierDemo.defaultVariable);
        System.out.println(modifierDemo.protectedVariable);
    }
}