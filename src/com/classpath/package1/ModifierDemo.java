package com.classpath.package1;

public class ModifierDemo {

    public int publicVariable = 10;
    private int privateVariable = 10;
    int defaultVariable = 120;
    protected int protectedVariable = 45;
}