package com.classpath.packages;

public class ArrayBackedStudentServiceImpl implements StudentService {

    private Student[] students = new Student[10];
    private static int index = 0;

    @Override
    public Student save(Student student) {

        if (index < 10) {
            this.students[index] = student;
            index++;
            return student;
        } else {
            throw new IllegalStateException("Array is full. Cannot store students");
        }
    }

    @Override
    public Student[] fetchAll() {
        return this.students;
    }

    @Override
    public Student findStudentById(long studentId) {
        for (Student student: students ) {
            if (student != null && student.getId() == studentId){
                return student;
            }
        }
        return null;
    }

    @Override
    public void deleteStudentById(long studentId) {
        for (Student student: students ) {
            if (student != null && student.getId() == studentId){
                student = null;
            }
        }
    }
}