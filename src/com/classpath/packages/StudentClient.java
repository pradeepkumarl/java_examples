package com.classpath.packages;

public class StudentClient {

    public static void main(String[] args) {

        StudentService studentService = new ArrayListBackedStudetnServiceImpl();

        Student ramesh = new Student("Ramesh", 12, 7 );
        Student suresh = new Student("Suresh", 10, 5 );
        Student krishna = new Student("Krishna", 11, 6 );
        Student hari = new Student("Hari", 12, 7 );
        Student vinod = new Student("Vinod", 9, 4 );

        studentService.save(ramesh);
        studentService.save(suresh);
        studentService.save(krishna);
        studentService.save(hari);
        studentService.save(vinod);

        Student[] students = studentService.fetchAll();

        for(int index = 0; index < 5; index ++){
            System.out.println(students[index].getName());
        }
    }
}