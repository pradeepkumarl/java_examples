package com.classpath.packages;

public interface StudentService {

    Student save(Student student);

    Student[] fetchAll();

    Student findStudentById(long studentId);

    void deleteStudentById(long studentId);
}