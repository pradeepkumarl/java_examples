package com.classpath.packages;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBackedStudetnServiceImpl implements StudentService{

    private List<Student> students = new ArrayList<>();

    @Override
    public Student save(Student student) {
         this.students.add(student);
         return student;
    }

    @Override
    public Student[] fetchAll() {
        return this.students.toArray(new Student[this.students.size()]);
    }

    @Override
    public Student findStudentById(long studentId) {
        for(Student student: students){
            if (student.getId() == studentId){
                return student;
            }
        }
        return null;
    }

    @Override
    public void deleteStudentById(long studentId) {
        for(Student student: students){
            if (student.getId() == studentId){
                this.students.remove(student);
            }
        }
    }
}