package com.classpath.collections;

import java.util.*;

public class ArrayListDemo {

    public static void main(String[] args) {
        //created a users list
        List<String> users = new LinkedList<>();

        //added users into the list. elements are added to the end of the list
        users.add("Rakesh");
        users.add("Ramesh");
        users.add("Suresh");
        users.add("Harish");
        users.add("Vishnu");


        //ways to iterate the list
       /* for(int index = 0; index < users.size(); index++){
            System.out.println(users.get(index));
        }*/

        //for-each
       /* for(Object user: users) {
            System.out.println(user);
        }*/

       /* Iterator<String> it = users.iterator();

        while(it.hasNext()){
            System.out.println(it.next().toUpperCase());
        }

        users.clear();

        System.out.println("Size of users after applying the clear method :: "+users.size());
*/

        ListIterator<String> listIterator = users.listIterator();

        System.out.println("==== Forward ====");
        while(listIterator.hasNext()){
            System.out.println(listIterator.next().toUpperCase());
        }

        System.out.println("==== Reverse ====");
        while(listIterator.hasPrevious()){
            System.out.println(listIterator.previous().toLowerCase());
        }

        List<Integer> values = new ArrayList<>();
        values.add(11);
        values.add(12);
        values.add(13);
        values.add(14);
        values.add(15);

        Integer[] valueArray = values.toArray(new Integer[values.size()]);


    }
}