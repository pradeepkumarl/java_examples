package com.classpath.collections;

import com.classpath.packages.Student;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

    public static void main(String[] args) {
        Set<Student> studentSet = new HashSet<>();

        Student ramesh = new Student("Ramesh", 12, 7 );
        Student suresh = new Student("Suresh", 10, 5 );
        Student krishna = new Student("Krishna", 11, 6 );
        Student hari = new Student("Hari", 12, 7 );
        Student vinod = new Student("Vinod", 9, 4 );
        ramesh.setAge(34);

        Student ramesh2 = new Student("Ramesh", 12, 7 );
        Student suresh2 = new Student("Suresh", 10, 5 );
        Student krishna2 = new Student("Krishna", 11, 6 );
        Student hari2 = new Student("Hari", 12, 7 );
        Student vinod2 = new Student("Vinod", 9, 4 );

        studentSet.add(ramesh);
        studentSet.add(suresh);
        studentSet.add(krishna);
        studentSet.add(hari);
        studentSet.add(vinod);

        studentSet.add(ramesh2);
        studentSet.add(suresh2);
        studentSet.add(krishna2);
        studentSet.add(hari2);
        studentSet.add(vinod2);


       // System.out.println("Size of studentsSets is "+studentSet.size());

        Student student = new Student("Ramesh", 22, 10);

        Student student2 = student;

        Student student3 = new Student("Ramesh", 22, 10);;


       /* System.out.println("Age of student 2 is "+student2.getAge());
        System.out.println("Age of student 3 is "+student3.getAge());*/
/*
        System.out.println("========= Object by references - start ==========");
        System.out.println("student == student2 " + (student == student2));
        System.out.println("student2 == student3 " + (student2 == student3));
        System.out.println("========= Object by references - end ==========");


        System.out.println( );


        System.out.println("========= Object by equality - start ==========");
        System.out.println("student equals student2 " + (student.equals(student2)));
        System.out.println("student2 equals student3 " + (student2.equals(student3)));
        System.out.println("========= Object by references - end ==========");

        System.out.println(student);*/


        Set<Student> set = new HashSet<>();
        Student ramesh3 = new Student("Ramesh", 12, 7 );
        set.add(ramesh3);
        ramesh3.setAge(21);

        System.out.println(set.contains(ramesh3));

    }
}