package com.classpath.collections;

import java.util.*;

public class SetDemo {

    public static void main(String[] args) {
        //created a users list
        Set<String> users = new HashSet<>();

        //added users into the list. elements are added to the end of the list
        users.add("Rakesh");
        users.add("Ramesh");
        users.add("Suresh");
        users.add("Harish");
        users.add("Vishnu");
        users.add("Vishnu");
        users.add("Vishnu");

        System.out.println("Size of set is "+users.size());

        Iterator<String> iterator = users.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}