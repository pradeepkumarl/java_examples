package com.classpath.collections;

import java.util.*;

public class MapDemo {

    public static void main(String[] args) {
        Map<String, String>  users = new HashMap<>();
        users.put("1", "Suresh");
        users.put("2", "Ramesh");
        users.put("3", "Harish");
        users.put("4", "Harsh");
        users.put("5", "Sunil");
        users.put("5", "Sunil");
        users.put("5", "Kundan");

        System.out.println(users.size());

        System.out.println(users.get("5"));


        //1. using the keys
        Set<String> keys = users.keySet();
        Iterator<String> keyIt = keys.iterator();
        while(keyIt.hasNext()){
            String id = keyIt.next();
            System.out.println(" Key :: "+ id + " Name : "+ users.get(id));
        }

        Collection<String> values = users.values();
        Iterator<String> usersIt = values.iterator();

        while(usersIt.hasNext()){
            System.out.println("2nd way to print values :: " +usersIt.next());
        }

        //3. get both keys and values
        Set<Map.Entry<String, String>> entrySet = users.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entrySet.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            System.out.println(" Key "+entry.getKey() + " Value: "+entry.getValue());
        }
        System.out.println(users.compute("1" , (k,v) -> v.toUpperCase() +"_"+k ));
    }
}