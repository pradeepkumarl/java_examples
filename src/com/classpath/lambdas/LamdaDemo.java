package com.classpath.lambdas;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LamdaDemo {

    public static void main(String[] args) {

        Product iMac = new Product(12, "IMac", "Apple", 2_00_000);
        Product vivo19 = new Product(13, "Vivo-Pro", "Vivo", 15_000);

        /*System.out.println(isApple.test(iMac));
        System.out.println(isNotApple.test(vivo19));

        System.out.println(isAppleAndLessThan50K.test(iMac));
        System.out.println(isAppleAndMoreThan50K.test(iMac));*/

       /* Stream<Product> productStream = IntStream
                .range(1, 10)
                .mapToObj(LamdaDemo::createProduct);

        productStream.forEach(product -> System.out.println(product));
*/
/*        Supplier<Product> productSupplier = () -> new Product(12, "IMac", "Apple", 2_00_000);
        Product product = productSupplier.get();*/

        Set<Product> productSet =  new HashSet<>();
        productSet.add(new Product(12, "IMac", "Apple", 2_00_000));
        productSet.add(new Product(13, "MacbookPro", "Apple", 2_25_000));
        productSet.add(new Product(14, "IPad", "Apple", 39_000));
        productSet.add(new Product(15, "IPhone", "Apple", 45_000));
        productSet.add(new Product(16, "Vivo", "Vivo", 20_000));




        long count = productSet
                .stream()
                .filter(isApple)
                .count();

        Stream<String> stringStream = productSet
                .stream()
                .filter(isApple.negate())
                .map(Product::getName)
                .map(String::toUpperCase)
                .limit(2);

       // stringStream.forEach(System.out::println);
        //.forEach(System.out::println);

/*
        Iterator<Product> productIterator = productSet.iterator();
        List<String> productNames = new ArrayList<>();
        while(productIterator.hasNext()){
            Product product = productIterator.next();
            if(product.getBrand().equalsIgnoreCase("Apple")){
                productNames.add(product.getName().toUpperCase());
            }
        }
        for(int i = 0 ;i < 2; i ++){
            System.out.println(productNames.get(i));
        }
*/
        //System.out.println("Number of apple products is "+ count);

        List<User> users = new ArrayList<>();

        User kiran = new User("Kiran");
        kiran.getPhoneNumbers().add("11111111");
        kiran.getPhoneNumbers().add("22222222");
        kiran.getPhoneNumbers().add("33333333");
        kiran.getPhoneNumbers().add("33333333");
        kiran.getPhoneNumbers().add("33333333");

        User vinay = new User("Vinay");
        vinay.getPhoneNumbers().add("44444444");
        vinay.getPhoneNumbers().add("55555555");
        vinay.getPhoneNumbers().add("55555555");
        vinay.getPhoneNumbers().add("55555555");
        vinay.getPhoneNumbers().add("66666666");

        users.add(kiran);
        users.add(vinay);

        Integer countOfNumbers  = users.stream()
                .flatMap(LamdaDemo::extractPhoneNumbers)
                .distinct()
                .map(Integer::parseInt)
                .reduce((s, s2) ->  s * s2)
                .orElse(0);

        Stream.of("1", "2", "3", "4")
                .map(Integer::parseInt)
                .map(x -> x * 5)
                .max(Integer::compareTo)
                .ifPresent(System.out::println);
        long count1 = Arrays.stream(new int[]{1, 2, 3, 4}).filter(x -> x > 2).count();
        System.out.println(count1);


        //System.out.println("The final count is "+ countOfNumbers);




    }

    static Predicate<Product>  isApple = (product) -> product.getBrand().equalsIgnoreCase("apple");

    static Predicate<Product>  isNotApple = isApple.negate();

    static Predicate<Product> priceLessThan50K = (product)-> product.getPrice() < 50_000;

    static  Predicate<Product> isAppleAndLessThan50K = isApple.and(priceLessThan50K);

    static  Predicate<Product> isAppleAndMoreThan50K = isAppleAndLessThan50K.negate();

    private static Product createProduct(int value) {
        return new Product(value, value + "-", "Apple", Math.ceil(Math.random() * 20000));
    }

    private static Stream<? extends String> extractPhoneNumbers(User user) {
        return user.getPhoneNumbers().stream();
    }
}

class Product {
    private int id;
    private String name;
    private String brand;

    private double price;

    public Product(int id, String name, String brand, double price ){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getBrand() {
        return brand;
    }
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return id == product.id && Double.compare(product.price, price) == 0 && Objects.equals(name, product.name) && Objects.equals(brand, product.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, brand, price);
    }
}

class User {
    private String name;
    private List<String> phoneNumbers;
    public User(String name){
        this.name = name;
        this.phoneNumbers = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void addPhoneNumber(String phoneNumber){
        this.phoneNumbers.add(phoneNumber);
    }
}

