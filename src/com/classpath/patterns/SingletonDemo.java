package com.classpath.patterns;

public class SingletonDemo {

    public static void main(String[] args) {
        GooglePay pay1 = GooglePay.getInstance();
        GooglePay pay2 = GooglePay.getInstance();
        System.out.println(" pay1 == pay2 " + (pay1 == pay2));
    }
}


class GooglePay {

    private GooglePay(){

    }
    private static GooglePay googlePay = new GooglePay();

    public static GooglePay getInstance(){
         /*if (googlePay == null){
             googlePay = new GooglePay();
         }*/
         return googlePay;
    }

    void processPayment(double amount){
        System.out.println("Processing the payment of "+ amount);
    }
}