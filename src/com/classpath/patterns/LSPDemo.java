package com.classpath.patterns;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LSPDemo {

    public static void main(String[] args) {
        List<Employee> list = new LinkedList<>();
        list.add(new Employee(12, "Ramesh"));
        list.add(new Employee(13, "Suresh"));
        list.add(new Employee(14, "Vishnu"));
        List<Employee> filteredEmployees = filterEmployeesById(list);
    }

    private static List<Employee> filterEmployeesById(List<Employee> employees) {
        List<Employee> filteredEmployees = employees.stream().filter(employee -> employee.getId()  < 13 ).collect(Collectors.toList());
        return filteredEmployees;
    }

}

class Employee {
    private long id;
    private String name;

    public Employee(long id, String name ){
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}