package com.classpath.patterns;

public class SRPDemo {

    public static void main(String[] args) {

        Student ramesh = new Student(12, "Ramesh");

        saveStudent(ramesh);

    }

    //save the student into the db
    private  static void saveStudent(Student student){
        // DBConnection connection = createDBConnection();
        // create the statement (query)
        validateStudent(student);
        // save the student
        // close the statement
        //close the connection
    }

    private static DBConnection createDBConnection(){
        return null;
    }

    private static  void validateStudent(Student student){

    }
}

class Student {
    private long id;
    private String name;

    public Student(long id, String name ){
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}