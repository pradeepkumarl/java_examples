package com.classpath.patterns;

public class OCPDemo {
}

interface PaymentGateway{

    void pay(String from, String to, double amount);
}

final class SavingsAccount {
    private long accountId;
    private String name;
    private double balance;

    public long getAccountId() {
        return accountId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double checkBalance() {
        return balance;
    }

    public final void withdraw(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount ){
        this.balance = this.balance + amount;
    }
}
