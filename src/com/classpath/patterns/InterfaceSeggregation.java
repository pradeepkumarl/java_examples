package com.classpath.patterns;

public class InterfaceSeggregation {

    public static void main(String[] args) {
        Passenger ramesh = new Passenger();
        ramesh.travel();
    }
}

class Passenger{
    Commute driver = new LuxuryCabService();
    void travel(){
        driver.commute("Home", "Airport");
    }

}

interface Commute {
    public void commute(String from, String to);
}

class Driver implements Commute {

    public void commute(String from, String to){
        System.out.println("Driving from "+from + " to "+ to);
    }
}

class LuxuryCabService implements Commute {
    public void commute(String from, String to){
        System.out.println("Driving in style from "+from + " to "+ to);
    }
}