package com.classpath.basic;

public class PaymentGatewayClient {

    public static void main(String[] args) {
        PaymentGateway gateway = new PhonePay();
        gateway.pay("Ramesh", "Suresh", 40_000, "Towards loan");
    }
}