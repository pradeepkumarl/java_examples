package com.classpath.basic;

public interface PaymentGateway {

    void pay (String from, String to, double amount, String notes);

}