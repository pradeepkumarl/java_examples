package com.classpath.basic;

import java.util.Scanner;

public class DoctorClient {

    public static void main(String[] args) {
        // class_name variable_name = new Class_name();
        // datatype variable_name = value;

        //com.classpath.basic.Doctor doctor = new com.classpath.basic.Doctor();
        //doctor.treatPatient();

        // RHS should always pass IS-A test with LHS

      //  Scanner scanner = new Scanner(System.in);
      //  System.out.println("Please enter the option 1 -> com.classpath.basic.Padietric, 2 -> com.classpath.basic.Ortho, 3 -> Knee Surgeon");

        /*int option = scanner.nextInt();
        Doctor doc = null;
        switch (option){
            case 1:
                doc = new Padietric();
                break;
            case 2:
                doc = new Ortho();
                break;
            case 3:
                doc = new KneeSurgeon();
                break;
            default:
                doc = new Padietric();
                break;
        }

        doc.treatPatient();
        scanner.close();*/

        //Using the LHS reference, you can only call the method on the LHS class

        Doctor doctor = new Ortho();

        Padietric padietric = new Padietric();
        if ( doctor instanceof Padietric) {
            padietric = (Padietric) doctor;
            padietric.treatKids();
        }else {
            System.out.println("Cannot cast to Padietric");
        }

    }
}

