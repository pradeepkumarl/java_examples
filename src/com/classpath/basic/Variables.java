package com.classpath.basic;

public class Variables {

    /**
     * This is used for documentation. These comments will be parsed by the document plugin to generate
     * documents.
     *
     * @param args
     */
    public static void main(String[] args) {
        // This is a single line comment
        int index = 0;
        /*
           Multi line comment. Multiple
           lines to be ignored, on the method, class etc
         */

        // Variable rules and conventions
        /*
          1. Case sensitive hello != Hello
          2. Variable names can contain letters, digits, $ and _  Hello$, index123, index_34
          3. Variable name cannot be a reserve word, if, switch, for, class, void
          4. Variable name cannot start with digit

          Conventions
          1. Use meaning full variable names, do not use i,j,k
          2. Use camelCase notation
          3. For method, variable use small characters and camelcase conventions
          4. For class names start with Captial letter and camelcase conventions
          5. For constants use ALL_CAPS
         */

        /* statement
            //declaration
           public int value;

           //assignment
           value = 56;

           public int value = 56;
         */

    }
}