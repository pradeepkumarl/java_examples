package com.classpath.basic;

public class Student {

    private long id;
    private String name;

    public Student(long id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Student student = new Student(12, "Ramesh");
        System.out.println(student);
    }
}