package com.classpath.basic;

public abstract class Doctor {

    private int age;

    private int experience;

    private String name;

    public abstract void treatPatient();
}

class Padietric extends Doctor {

    public void treatKids (){
        System.out.println("Treating kids ..");
    }

    public  void treatPatient(){
       treatKids();
    }
}

class Ortho extends Doctor {
    public void conductXRay(){
        System.out.println("Conduting X-Ray");
    }

    public void conductCTScan(){
        System.out.println("Conducting CT-Scan");
    }

    @Override
    public void treatPatient(){
        conductXRay();
        conductCTScan();
    }
}

class KneeSurgeon extends Ortho {
    public void conductKneeSurgery(){
        System.out.println("Conducting Knee Surgery ..");
    }

    @Override
    public void treatPatient(){
        conductXRay();
        conductCTScan();
        conductKneeSurgery();
    }
}

class Optholmologist extends Doctor {

    @Override
    public void treatPatient() {
        System.out.println("Treating eyes");
    }
}