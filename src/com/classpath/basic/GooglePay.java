package com.classpath.basic;

public class GooglePay implements PaymentGateway{

    @Override
    public void pay(String from, String to, double amount, String notes) {
        System.out.printf( "Sending money using GPay from %s to %s for the amount %s and notes: %s", from, to, amount, notes);
    }

    public int rewardPoints(){
        return (int)Math.ceil(Math.random());
    }
}

class PhonePay implements PaymentGateway {

    @Override
    public void pay(String from, String to, double amount, String notes) {
        System.out.printf( "Sending money using com.classpath.basic.PhonePay from %s to %s for the amount %s and notes: %s", from, to, amount, notes);
    }
}
class NetBanking implements PaymentGateway {

    @Override
    public void pay(String from, String to, double amount, String notes) {
        System.out.printf( "Sending money using com.classpath.basic.NetBanking from %s to %s for the amount %s and notes: %s", from, to, amount, notes);
    }
}
class PayTM implements PaymentGateway {

    @Override
    public void pay(String from, String to, double amount, String notes) {
        System.out.printf( "Sending money using PayTm from %s to %s for the amount %s and notes: %s", from, to, amount, notes);
    }
}

