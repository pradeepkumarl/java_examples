package com.classpath.package2;

import com.classpath.package1.ModifierDemo;
import com.classpath.package1.Package1Client;

public class Package2Client extends ModifierDemo{

    public static void main(String[] args) {
        ModifierDemo demo = new ModifierDemo();
        System.out.println(demo.publicVariable);
        Package2Client client = new Package2Client();
        System.out.println(client.protectedVariable);;
        System.out.println(client.publicVariable);;
    }
}